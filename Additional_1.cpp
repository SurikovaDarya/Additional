﻿#include <iostream>

int DegreeOfTwo(int n)
{
    if (n == 1) return true;
    if (n > 1 && n % 2 == 0)
    {
        n = n / 2;
        return DegreeOfTwo(n);
    }
    else
        return false;
}

int main()

{
    int a;
    std::cin >> a;
    std::cout << DegreeOfTwo(a);

}


